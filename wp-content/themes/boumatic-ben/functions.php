<?php

//	Favorite Icon
function childtheme_favicon() {
?>
    <link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/img/favicon.ico">

<?php
}

add_action('wp_head', 'childtheme_favicon');


//	Print css
function childtheme_header() { 
?>
    <link type="text/css" media="print" rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" />
	<meta name="viewport" content="initial-scale:1.0,width=device-width" />
<?php
}
 
add_action('wp_head', 'childtheme_header');

//	Custom IE Style Sheet
function childtheme_iefix() { 
?>
    <!--[if lte IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('stylesheet_directory') ?>/ie.css" />
    <![endif]-->
<?php 
}
 
//add_action('wp_head', 'childtheme_iefix');

//	Overides default FULL SIZE image size
$GLOBALS['content_width'] = 550;
function childtheme_override_blogdescription()
{
    return '';
}
//	Child Theme Menu
function childtheme_menu() {
        //$stylesheet = bloginfo('stylesheet_directory');
	$menu .= '<div class="menu"><ul>';
	// Note the include below. 
	// It only shows pages with those IDs. Change the ID number to whatever you like.
	$menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_categories('title_li=&sort_column=menu_order&echo=0&hierarchical=true') );
	$menu .= '</li></ul></div>';
    echo $menu;
}
add_filter( 'wp_page_menu', 'childtheme_menu' );

// Custom Homepage header

function homePage_Header() {
?>
    <h1 class="page-title">Newest Recipes</h1>
<?php
} 

add_action('thematic_above_indexloop', 'homePage_Header');

//	Custom Post Header
function childtheme_postheader() {
    global $post; 
    if (is_page()) {
    ?>
        <h2 class="entry-title"><?php the_title(); ?></h2>        

    <?php 
    } elseif (is_404()) {
    ?>
        <h2 class="entry-title">Not Found</h2>        
    <?php 
    } elseif (is_single()) {
    ?>
        <h2 class="entry-title"><?php the_title(); ?></h2>
    <?php
    } else {
    ?>
        <h2 class="entry-title"><a href="<?php the_permalink() ?>" title="<?php printf(__('Permalink to %s', 'thematic'), wp_specialchars(get_the_title(), 1)) ?>" rel="bookmark"><?php the_title() ?></a></h2>
		
        <?php
        if ($post->post_type == 'post') { // Hide entry meta on searches
        } 

    }
}

add_filter ('thematic_postheader', 'childtheme_postheader');
 
// ADD FOOTER TAXONOMY TO POST HEADER


function my_postfooter($content) {
    
         global $id, $post;

    // Create $posteditlink    
    $posteditlink .= '<span class="edit"><a href="' . get_bloginfo('wpurl') . '/wp-admin/post.php?action=edit&amp;post=' . $id;
    $posteditlink .= '" title="' . __('Edit post', 'thematic') .'">';
    $posteditlink .= __('Edit', 'thematic') . '</a></span>';
    $posteditlink = apply_filters('thematic_postfooter_posteditlink',$posteditlink); 
    
    // Display the post categories  
    $postcategory .= '<span class="cat-links">';
    if (is_single() || is_tag() || is_home()) {
        $postcategory .= __('', 'thematic') . get_the_category_list(' ');
        $postcategory .= '</span>';
    } elseif ( is_category() && $cats_meow = thematic_cats_meow('') ) { /* Returns categories other than the one queried */
        $postcategory .= __('', 'thematic') . $cats_meow;
        $postcategory .= '</span>';
    } else {
        $postcategory .= '</span>';
    }
    $postcategory = apply_filters('thematic_postfooter_postcategory',$postcategory); 
    
    // Display the tags
    if (is_single()) {
        $tagtext = __(' and tagged', 'thematic');
        $posttags = get_the_tag_list("<span class=\"tag-links\"> $tagtext ",'','</span>');
    } elseif ( is_tag() && $tag_ur_it = thematic_tag_ur_it('') ) { /* Returns tags other than the one queried */
        $posttags = '<span class="tag-links">' . __(' Also tagged ', 'thematic') . $tag_ur_it . '</span>';
    } else {
        $tagtext = __('Tagged', 'thematic');
        $posttags = get_the_tag_list("<span class=\"tag-links\"> $tagtext ",'','</span>');
    }
    $posttags = apply_filters('thematic_postfooter_posttags',$posttags); 
    
    // Display comments link and edit link
    if (comments_open()) {
        $postcommentnumber = get_comments_number();
        if ($postcommentnumber > '1') {
            $postcomments = '<span class="comments-link"><a href="' . get_permalink() . '#comments" title="' . __('Comment on ', 'thematic') . the_title_attribute('echo=0') . '">';
            $postcomments .= get_comments_number() . __(' Comments', 'thematic') . '</a></span>';
        } elseif ($postcommentnumber == '1') {
            $postcomments = '<span class="comments-link"><a href="' . get_permalink() . '#comments" title="' . __('Comment on ', 'thematic') . the_title_attribute('echo=0') . '">';
            $postcomments .= get_comments_number() . __(' Comment', 'thematic') . '</a></span>';
        } elseif ($postcommentnumber == '0') {
            $postcomments = '<span class="comments-link"><a href="' . get_permalink() . '#comments" title="' . __('Comment on ', 'thematic') . the_title_attribute('echo=0') . '">';
            $postcomments .= __('Leave a comment', 'thematic') . '</a></span>';
        }
    } else {
        $postcomments = ' <span class="comments-link comments-closed-link">' . __('Comments closed', 'thematic') .'</span>';
    }
    // Display edit link
    if (current_user_can('edit_posts')) {
        $postcomments .=  $posteditlink;
    }               
    $postcomments = apply_filters('thematic_postfooter_postcomments',$postcomments); 
    
    // Display permalink, comments link, and RSS on single posts
    if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) { /* Comments are open */
        $postconnect .= ' <a class="comment-link" href="#respond" title ="' . __('Post a comment', 'thematic') . '">' . __('Post a comment', 'thematic') . '</a>';
        $postconnect .= __(' or leave a trackback: ', 'thematic');
        $postconnect .= '<a class="trackback-link" href="' . trackback_url(FALSE) . '" title ="' . __('Trackback URL for your post', 'thematic') . '" rel="trackback">' . __('Trackback URL', 'thematic') . '</a>.';
    } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) { /* Only trackbacks are open */
        $postconnect .= __(' Comments are closed, but you can leave a trackback: ', 'thematic');
        $postconnect .= '<a class="trackback-link" href="' . trackback_url(FALSE) . '" title ="' . __('Trackback URL for your post', 'thematic') . '" rel="trackback">' . __('Trackback URL', 'thematic') . '</a>.';
    } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) { /* Only comments open */
        $postconnect .= __(' Trackbacks are closed, but you can ', 'thematic');
        $postconnect .= '<a class="comment-link" href="#respond" title ="' . __('Post a comment', 'thematic') . '">' . __('post a comment', 'thematic') . '</a>.';
    } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) { /* Comments and trackbacks closed */
        $postconnect .= __(' Both comments and trackbacks are currently closed.', 'thematic');
    }
    // Display edit link on single posts
    if (current_user_can('edit_posts')) {
        $postconnect .= ' ' . $posteditlink;
    }
    $postconnect = apply_filters('thematic_postfooter_postconnect',$postconnect); 
    
    
    // Add it all up
    if ($post->post_type == 'page' && current_user_can('edit_posts')) { /* For logged-in "page" search results */
        $postfooter = '<div class="entry-utility">' . '<span class="edit">' . $posteditlink . '</span>';
        $postfooter .= "</div><!-- .entry-utility -->\n";    
    } elseif ($post->post_type == 'page') { /* For logged-out "page" search results */
        $postfooter = '';
    } else {
        if (is_single()) {
            $postfooter = '<div class="entry-utility">' . $postcategory . $posttags;
			$postfooter .= "</div><!-- .entry-utility -->\n"; 
        } else {
          //  $postfooter = '<div class="entry-utility">'. $postcategory . $posttags . $postcomments;
        }
           
    }
    
    // Put it on the screen
    echo  $postfooter; // Filter to override default post footer
} // end thematic_postfooter

add_filter('thematic_postheader', 'my_postfooter');
	
//	Remove postfooter
function remove_footer() {
	
	echo '';
	}
add_action('thematic_postfooter','remove_footer',5);

function remove_bottom_nav() {
    //global $id, $post;
    //if( is_category() || is_archive() ){
        //echo "CAT";
        //remove_action('thematic_navigation_below', 'thematic_nav_below', 2);
	remove_action('thematic_navigation_above', 'thematic_nav_above', 2);
    //}
}
add_action('init', 'remove_bottom_nav');

//	Custom BLOG TITLE

function remove_thematic_blogtitle() {
	
	remove_action('thematic_header','thematic_blogtitle', 3);
	
	}

add_action('init', 'remove_thematic_blogtitle');

	
function new_thematic_blog_title() {
	?>
	
    <div id="blog-title"><a href="<?php bloginfo('url') ?>/" title="<?php bloginfo('name') ?>" rel="home"><span><?php bloginfo('name') ?></span></a></div>
	
    	<?php 
}

add_action('thematic_header','new_thematic_blog_title',3);

// The Index Loop


function childtheme_override_index_loop() {
		/// Count the number of posts so we can insert a widgetized area
    global $options, $blog_id;

    foreach ($options as $value) {
        if (get_option( $value['id'] ) === FALSE) { 
            $$value['id'] = $value['std']; 
        } else {
            if (THEMATIC_MB) 
            {
                    $$value['id'] = get_option($blog_id,  $value['id'] );
            }
            else
            {
                    $$value['id'] = get_option( $value['id'] );
            }
        }
    }          
    $count = 1;
    $page = get_query_var('paged');

    while ( have_posts() ) : the_post() ?>

        <?php

        // add extra class "odd" to every other post on homepage
        $odd = "";
        $features = 1;

        if ($count > 1 && is_float( $count / 2 )) :
            $odd = " odd";
        endif; ?>
        <div class="<?php thematic_post_class() ?><?php echo $odd ?>">
            <div id="post-<?php the_ID() ?>" >

            <?php thematic_postheader(); ?>
                    <div class="entry-content">
                    <?php if ($features >= $count && $page == 0){ // show full posts for number of $features

                        thematic_content();

                    } elseif($page == 0){

                        the_excerpt();

                    } else {
                       // none();
                    }
                    ?>
                    <?php wp_link_pages('before=<div class="page-link">' .__('Pages:', 'thematic') . '&after=</div>') ?>
                    </div>
            <?php thematic_postfooter(); ?>
            </div><!-- .post -->
        </div>

                        <?php comments_template();

                        if ($count==$thm_insert_position) {
                                        get_sidebar('index-insert');
                        }
                        $count = $count + 1;
    endwhile;
}




function childtheme_override_content_init()  {
       // echo "childtheme init";
        global $thematic_content_length;

        $content = '';
        $thematic_content_length = '';

        if (is_home() || is_front_page()) {
                $content = 'excerpt';
        } elseif (is_single()) {
                $content = 'full';
        } elseif (is_tag()) {
                $content = 'none';
        } elseif (is_search()) {
                $content = 'excerpt';
        } elseif (is_category()) {
                $content = 'none';
        } elseif (is_author()) {
                $content = 'excerpt';
        } elseif (is_archive()) {
                $content = 'excerpt';
        }

        $thematic_content_length = apply_filters('thematic_content', $content);

}
add_action('thematic_abovepost','thematic_content_init');

function childtheme_override_category_loop() {

    // this function ....


    $postCount = '999';

    $cats = array();
    
    //The Query
    $cat = get_query_var('cat');
    $args=array(
            'orderby' => 'title',
            'order' => 'ASC',
            'posts_per_page' => $postCount,
            'cat' => $cat
            );
    $my_init_query = new WP_Query($args);
    while ($my_init_query->have_posts()) : $my_init_query->the_post();

        $categories = get_the_category();
        foreach ($categories as $cat){
            
            array_push(&$cats, $cat->cat_ID);
        }
                //print_r($category);
    endwhile;
    $my_init_query->rewind_posts();
    // we now have an array of current and descendant catids for the current category
 

    $numPosts = count($cats);

    $cats = array_values(array_unique($cats));


    $numCats = count($cats);
    if($numCats > 1){
        $catText = "Categories";
    } else {
        $numCats = "this";
        $catText = "Category";
    }

    echo "<p>There's ".$numPosts. " Recipes in ".$numCats." ".$catText."</p>";

    for($i = 0; $i < $numPosts; $i++)
    {
        
        if(isset($cats[$i]))
        {
            echo "<h3 class=\"category\">".get_the_category_by_ID($cats[$i])."</h3>";
            //echo "<br />".$cats[$i]."<br /><br />";
            
            if(get_categories('child_of='.$cats[$i]))
            {//parent cat;
                
                 $args=array(
                    'orderby' => 'title',
                    'order' => 'ASC',
                    'posts_per_page' => $postCount,
                    'category__in' => array($cats[$i])

                );

             } else {//child cat;
                 
                 $args=array(
                    'orderby' => 'title',
                    'order' => 'ASC',
                    'posts_per_page' => $postCount,
                    'cat' => $cats[$i]
                );
             }
             $my_query = new WP_Query($args);

             while ($my_query->have_posts()) : $my_query->the_post();

            // loop through each category's posts
            //
            //print_r($my_query);
             thematic_abovepost();
             ?>
                <div id="post-<?php the_ID();
                    echo '" ';
                    if (!(THEMATIC_COMPATIBLE_POST_CLASS))
                    {
                        post_class();
                        echo '>';
                    } else {
                        echo 'class="';
                        thematic_post_class();
                        echo '">';
                    }
                            //echo the_category();
                    thematic_postheader(); ?>
                    <div class="entry-content">
                        <?php// thematic_content(); ?>
                    </div><!-- .entry-content -->
                    <?php thematic_postfooter(); ?>
                </div><!-- #post -->

                <?php

                thematic_belowpost();

            endwhile;

            
        }
    }
         
}
 // end category_loop
function childtheme_override_nav_below() {
    if (is_single()) {

        
        ?>

        <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php thematic_previous_post_link() ?></div>
                <div class="nav-next"><?php thematic_next_post_link() ?></div>
        </div>

<?php
    } else if (is_category()) {

  
    } else {
    ?>

        <div id="nav-below" class="navigation">
            <?php
            if(function_exists('wp_pagenavi')) { ?>

                <?php wp_pagenavi(); ?>
                <?php

            } else { ?>
                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&laquo;</span> Older posts', 'thematic')) ?></div>
                <div class="nav-next"><?php previous_posts_link(__('<span class="meta-nav">&raquo;</span> Newer posts', 'thematic')) ?></div>
            <?php

            } ?>
        </div>

    <?php
    }
}
function childtheme_override_previous_post_link() {
    $args = array ('format'              => '%link',
                    'link'                => '<span class="metass-nav">&laquo;</span> %title',
                    'in_same_cat'         => TRUE,
                    'excluded_categories' => '');
    $args = apply_filters('thematic_previous_post_link_args', $args );
    previous_post_link($args['format'], $args['link'], $args['in_same_cat'], $args['excluded_categories']);

}
function childtheme_override_next_post_link() {
    $args = array ('format'              => '%link',
                    'link'                => '<span class="metass-nav">&raquo;</span> %title',
                    'in_same_cat'         => TRUE,
                    'excluded_categories' => '');
    $args = apply_filters('thematic_next_post_link_args', $args );
    next_post_link($args['format'], $args['link'], $args['in_same_cat'], $args['excluded_categories']);
}
function childtheme_override_page_title() {

    global $post;

    $content = '';
    if (is_attachment()) {
                    $content .= '<h2 class="page-title"><a href="';
                    $content .= apply_filters('the_permalink',get_permalink($post->post_parent));
                    $content .= '" rev="attachment"><span class="meta-nav">&laquo; </span>';
                    $content .= get_the_title($post->post_parent);
                    $content .= '</a></h2>';
    } elseif (is_author()) {
                    $content .= '<h1 class="page-title author">';
                    $author = get_the_author_meta( 'display_name' );
                    $content .= __('Author Archives: ', 'thematic');
                    $content .= '<span>';
                    $content .= $author;
                    $content .= '</span></h1>';
    } elseif (is_category()) {
                    $content .= '<h1 class="page-title">';
                    $content .= __('', 'thematic');
                    $content .= ' <span>';
                    $content .= single_cat_title('', FALSE);
                    $content .= '</span></h1>' . "\n";
                    $content .= '<div class="archive-meta">';
                    if ( !(''== category_description()) ) : $content .= apply_filters('archive_meta', category_description()); endif;
                    $content .= '</div>';
    } elseif (is_search()) {
                    $content .= '<h1 class="page-title">';
                    $content .= __('Search Results for:', 'thematic');
                    $content .= ' <span id="search-terms">';
                    $content .= esc_html(stripslashes($_GET['s']));
                    $content .= '</span></h1>';
    } elseif (is_tag()) {
                    $content .= '<h1 class="page-title">';
                    $content .= __('Tag:', 'thematic');
                    $content .= ' <span>';
                    $content .= __(thematic_tag_query());
                    $content .= '</span></h1>';
    } elseif (is_tax()) {
                global $taxonomy;
                    $content .= '<h1 class="page-title">';
                    $tax = get_taxonomy($taxonomy);
                    $content .= $tax->labels->name . ' ';
                    $content .= __('Archives:', 'thematic');
                    $content .= ' <span>';
                    $content .= thematic_get_term_name();
                    $content .= '</span></h1>';
    }	elseif (is_day()) {
                    $content .= '<h1 class="page-title">';
                    $content .= sprintf(__('Daily Archives: <span>%s</span>', 'thematic'), get_the_time(get_option('date_format')));
                    $content .= '</h1>';
    } elseif (is_month()) {
                    $content .= '<h1 class="page-title">';
                    $content .= sprintf(__('Monthly Archives: <span>%s</span>', 'thematic'), get_the_time('F Y'));
                    $content .= '</h1>';
    } elseif (is_year()) {
                    $content .= '<h1 class="page-title">';
                    $content .= sprintf(__('Yearly Archives: <span>%s</span>', 'thematic'), get_the_time('Y'));
                    $content .= '</h1>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
                    $content .= '<h1 class="page-title">';
                    $content .= __('Blog Archives', 'thematic');
                    $content .= '</h1>';
    }
    $content .= "\n";
    echo apply_filters('thematic_page_title', $content);
}

// Debug Function

if ( ! function_exists('debug'))
{
  function debug($var, $fn='print_r')
  {
    if (function_exists($fn))
    {
      echo '<pre>', $fn($var), '</pre>';
    }
    else
    {
      echo '<pre>The function ', $fn, '() does not exist. Falling back to',
      ' print_r().<br /><br />', print_r($var), '</pre>';
    }
  }
}



?>